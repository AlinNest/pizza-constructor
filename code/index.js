import { pizzaSelectSize, show, validate, random, bannerMove } from "./functions.js";
import { pizzaUser, pizzaBD } from "./data-pizza.js"

/*Доробити сайт піцерії.
Реалізувати перетягування інгредієнтів на корж піци з вказанням даних за ціною та назві топінгу.
Зробити так, щоб кнопка знижки втікала від користувача.*/

document.querySelectorAll(".grid input")
    .forEach((input) => {
        if (input.type === "text" || input.type === "tel" || input.type === "email") {
            input.addEventListener("change", () => {
                if (input.type === "text" && validate(/^[А-я-іїґє]{2,}$/i, input.value)) {
                    selectInput(input, pizzaUser)
                } else if (input.type === "tel" && validate(/^\+380\d{9}$/, input.value)) {
                    selectInput(input, pizzaUser)
                } else if (input.type === "email" && validate(/^[a-z0-9_.]{3,}@[a-z0-9._]{2,}\.[a-z.]{2,9}$/i, input.value)) {
                    selectInput(input, pizzaUser)
                } else {
                    input.classList.add("error")
                }
            })
        } else if (input.type === "reset") {
            input.addEventListener("click", () => {

            })
        } else if (input.type === "button") {
            input.addEventListener("click", () => {
                localStorage.userInfo = JSON.stringify(pizzaUser);
            })
        }
    })

function selectInput(input, data) {
    input.className = ""
    input.classList.add("success")
    data.userName = input.value
}

document.querySelector("#pizza")
    .addEventListener("click", pizzaSelectSize)

show(pizzaUser)


document.querySelector('.ingridients').addEventListener('dragstart',
    function (evt) {
      evt.dataTransfer.effectAllowed = 'move';
      evt.dataTransfer.setData('Text', evt.target.src);
      evt.dataTransfer.setData('idText', evt.target.id);    
    },
    false
  );

document.querySelector(".table").addEventListener("dragover", function (evt) {
    if (evt.preventDefault) evt.preventDefault();
    return false;
}, false);

document.querySelector(".table").addEventListener("drop", function (evt) {
    if (evt.preventDefault) evt.preventDefault();
    if (evt.stopPropagation) evt.stopPropagation();

    let src = evt.dataTransfer.getData("Text"); 
    let idEl = evt.dataTransfer.getData("idText"); 
    

  if (idEl === 'sauceClassic' || idEl === 'sauceRikotta' || idEl === 'sauceBBQ') {
    if (document.querySelector(`.sauceNew`)) {
      document.querySelector(`.sauceNew`).remove();
    }
    const img = document.createElement('img');
    img.classList.add('sauceNew');
    img.src = src;
    document.querySelector(".table").appendChild(img);
  }else {
    const img = document.createElement('img');
    img.src = src;
    img.classList.add(`${idEl}`);
    
    document.querySelector(".table").appendChild(img);
}

    switch (idEl) {
        case "sauceClassic" : pizzaUser.sauce = pizzaBD.sauce.find(el => el.name === idEl);
        break;
        case "sauceBBQ" : pizzaUser.sauce = pizzaBD.sauce.find(el => el.name === idEl);
        break;
        case "sauceRikotta" : pizzaUser.sauce = pizzaBD.sauce.find(el => el.name === idEl);
        break;
        case "moc1" : pizzaUser.add(pizzaBD.topping.find(el => el.name === idEl));
        break;
        case "moc2" : pizzaUser.add(pizzaBD.topping.find(el => el.name === idEl));
        break;
        case "moc3" : pizzaUser.add(pizzaBD.topping.find(el => el.name === idEl));
        break;
        case "telya" : pizzaUser.add(pizzaBD.topping.find(el => el.name === idEl));
        break;
        case "vetch1" : pizzaUser.add(pizzaBD.topping.find(el => el.name === idEl));
        break;
        case "vetch2" : pizzaUser.add(pizzaBD.topping.find(el => el.name === idEl));
        break;
    }
    show(pizzaUser);

   return false; 
     }, false);

document.querySelector("#banner").addEventListener("mouseenter", bannerMove);




    

