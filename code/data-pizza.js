export const pizzaBD = {
    size: [
        { name: "small", price: 40 },
        { name: "mid", price: 55 },
        { name: "big", price: 70 }
    ],
    topping: [
        { name: "moc1", price: 40, productName: "Сир звичайний"},
        { name: "moc2", price: 40, productName: "Сир фета"},
        { name: "moc3", price: 40, productName: "Моцарелла"},
        { name: "telya", price: 65, productName: "Телятина"},
        { name: "vetch1", price: 35, productName: "Помiдори"},
        { name: "vetch2", price: 37, productName: "Гриби"}
    ],
    sauce: [
        { name: "sauceClassic", price: 30, productName: "Кетчуп" },
        { name: "sauceBBQ", price: 30, productName: "BBQ" },
        { name: "sauceRikotta", price: 30, productName: "Рiкотта" }
    ]
}

export const pizzaUser = {
    size: { name: "big", price: 70 },
    sauce: "", 
    topping: [],
    price: "",
    userPhone: "",
    userEmail: "",
    userName: "",
    data: "",
    add(product) {
        for (const item of this.topping) {
            if(product.name === item.name){
                item.quantity += 1;
                return;
            }
           } 
           const newTopping = {
            ...product,
            quantity: 1
           }
           this.topping.push(newTopping);
    },
    remove(product){
        for (let i = 0; i < this.topping.length; i+= 1) {
            if(product === this.topping[i].name){
                this.topping.splice(i, 1);
            }
           } 
    }
}
