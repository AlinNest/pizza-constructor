import { pizzaUser, pizzaBD } from "./data-pizza.js";

const pizzaSelectSize = (e) => {
    if(e.target.tagName === "INPUT" && e.target.checked){
        pizzaUser.size = pizzaBD.size.find(el=>el.name === e.target.id);
    }
    show(pizzaUser)
};

function show (pizza) {
    const price = document.getElementById("price");
    const sauce = document.getElementById("sauce");
    const topping = document.getElementById("topping");
    

    let totalPrice = 0;
    if(pizza.size !== ""){
        totalPrice += parseFloat(pizza.size.price);
    }
    if(pizza.sauce !== ""){
        totalPrice += parseFloat(pizza.sauce.price);
    }
    if(pizza.topping.length){
        totalPrice += pizza.topping.reduce((a,b)=>a + b.price*b.quantity, 0);
        
    }
    price.innerText = totalPrice;
   
    if(pizza.sauce !== ""){
        sauce.innerHTML = `<div><span class="topping">${pizza.sauce.productName}</span></div>`
    }

  
    if(Array.isArray(pizza.topping)){
   topping.innerHTML = pizza.topping.map(el => `<div id='${el.name}'><span  class="topping">${el.productName}</span><span class="count">${el.quantity}</span> <button id='${el.name}' class='removeTopping'>&#10060</button></div>`).join('');
    }

    pizzaUser.price = totalPrice;
    pizzaUser.data = new Date();

    const btnRem = document.querySelectorAll(".removeTopping");
    if(btnRem) {
        btnRem.forEach((el) => {
            el.addEventListener("click", function (e) {
                for (let i = 0; i < pizzaUser.topping.length; i++) {
                    if(e.target.id === pizzaUser.topping[i].name) {
                        pizzaUser.topping[i].quantity -= 1;
                        
                       if(document.querySelector(`#${e.target.id}`)) {
                         document.querySelector(`.${e.target.id}`).remove();
                        }
                        show(pizzaUser);
                    
                        if(pizzaUser.topping[i].quantity === 0) {
                            pizzaUser.remove(e.target.id);
                            document.querySelector(`div#${e.target.id}`).remove();
                            document.querySelectorAll(`.${e.target.id}`).forEach((el) => el.remove());
                        }
                    }
                    
                }
            });
        })
    }
}

const validate = (pattern, value) => pattern.test(value);

const random = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

function bannerMove()  {
    const bannerEl = document.querySelector("#banner"); 
    bannerEl.classList.remove('bannerPos');
    bannerEl.style.left = `${random(0, 90)}%`;
    bannerEl.style.top = `${random(0, 90)}%`;
}


export {pizzaSelectSize, show, validate, random, bannerMove } 